#!/usr/bin/python3

import time
import socket
import json
import datetime

UDP_IP = "127.0.0.1"
UDP_PORT = 6004

# rpi_pixhawk = {'start_video_transmission': '0', 'start_video_transmission_timestamp': 0, 'start_video_transmission_wait':[5,60],
#               'system_error_state_handler': '0', 'system_error_state_handler_timestamp': 0, 'system_error_state_handler_wait': [6,60],
#               'watch_connection': '0', 'watch_connection_timestamp': 0, 'watch_connection_wait': [4,60],
#               'send_receive_drone_commands': '0', 'send_receive_drone_commands_timestamp': 0, 'send_receive_drone_commands_wait': [4,60],
#               'reply_all_data': '0', 'reply_all_data_timestamp': 0, 'reply_all_data_wait': [4,60],
#               'MAIN_cycling': '0', 'MAIN_cycling_timestamp': 0, 'MAIN_cycling_wait': [10,60]
#               }

# smth_wait: if ok, show more than 4. if more than 60s, remove from showing
rpi_sensors = {'fence': '0', 'fence_timestamp': 0,  'fence_wait': [4,60],
               'cell': '0', 'cell_timestamp': 0, 'cell_wait': [4,120],
               'batt_ah': '0', 'batt_ah_timestamp': 0, 'batt_ah_wait': [4,120],
               'thread': '0', 'thread_timestamp': 0, 'thread_wait': [4,200],
               'system_error_state_handler': '0', 'system_error_state_handler_timestamp': 0, 'system_error_state_handler_wait': [6,60],
               'MAIN_cycling': '0', 'MAIN_cycling_timestamp': 0, 'MAIN_cycling_wait': [4,60],
               'voltage_data_read': '0', 'voltage_data_read_timestamp': 0, 'voltage_data_read_wait': [4,60],
               'current_data_read': '', 'current_data_read_timestamp': 0, 'current_data_read_wait': [4,60],
               'gather_pixhawk_data': '', 'gather_pixhawk_data_timestamp': 0, 'gather_pixhawk_data_wait': [4,60],
               'geo_fencing': '', 'geo_fencing_timestamp': 0, 'geo_fencing_wait': [3,60],
               'reply_all_data': '', 'reply_all_data_timestamp': 0, 'reply_all_data_wait': [4,60],
               'send_drone_commands': '', 'send_drone_commands_timestamp': 0, 'send_drone_commands_wait': [3,60],
               'watch_connection': '', 'watch_connection_timestamp': 0, 'watch_connection_wait': [3,60],
               'set_camera_pitch_angle': '', 'set_camera_pitch_angle_timestamp': 0, 'set_camera_pitch_angle_wait': [3,60],
               'update_antenna_tracker': '', 'update_antenna_tracker_timestamp': 0, 'update_antenna_tracker_wait': [3,60],
               'camera_angle_controll_Main': '', 'camera_angle_controll_Main_timestamp': 0, 'camera_angle_controll_Main_wait': [3,60],
               'start_video_transmission': '', 'start_video_transmission_timestamp': 0, 'start_video_transmission_wait': [4,60],
               'watch_rc_inputs': '0', 'watch_rc_inputs_timestamp': 0, 'watch_rc_inputs_wait': [4, 60]
               }

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP, UDP_PORT))


def draw_color(color):
    if color:
        return "\033[1;31;40m"
    else:
        return "\033[1;37;40m"


while True:
    error_data, sending_ip = sock.recvfrom(1024)
    try:
        data_array = json.loads(error_data)
    except:
        data_array = None

    now = int(time.time() * 1000)

    if isinstance(data_array, dict):
        if 'error' in data_array.keys():
            if 'rpi_sensors' in data_array['error']:
                if 'system_error_state_handler' in data_array['error']['rpi_sensors']:
                    if 'Fence breach!!!' in data_array['error']['rpi_sensors'][1]:
                        rpi_sensors['fence'] = data_array['error']['rpi_sensors'][1:]
                        rpi_sensors['fence_timestamp'] = now
                    elif 'cell ' in data_array['error']['rpi_sensors'][1]:
                        rpi_sensors['cell'] = data_array['error']['rpi_sensors'][1:]
                        rpi_sensors['cell_timestamp'] = now
                    elif 'used batt ah' in data_array['error']['rpi_sensors'][1]:
                        rpi_sensors['batt_ah'] = data_array['error']['rpi_sensors'][1:]
                        rpi_sensors['batt_ah_timestamp'] = now
                    elif 'thread' in data_array['error']['rpi_sensors'][1]:
                        rpi_sensors['thread'] = data_array['error']['rpi_sensors'][1:]
                        rpi_sensors['thread_timestamp'] = now
                    else:
                        rpi_sensors['system_error_state_handler'] = data_array['error']['rpi_sensors'][1:]
                        rpi_sensors['system_error_state_handler_timestamp'] = now
                elif data_array['error']['rpi_sensors'][0] in rpi_sensors.keys():
                    rpi_sensors[data_array['error']['rpi_sensors'][0]] = data_array['error']['rpi_sensors'][1:None]
                    rpi_sensors[str(data_array['error']['rpi_sensors'][0])+'_timestamp'] = now
                else:
                    print('unknown rpi_sensors error reporting thread')
                    print(data_array)

            # elif 'rpi_pixhawk' in data_array['error']:
            #     if data_array['error']['rpi_pixhawk'][0] in rpi_pixhawk.keys():
            #         rpi_pixhawk[data_array['error']['rpi_pixhawk'][0]] = data_array['error']['rpi_pixhawk'][1:None]
            #         rpi_pixhawk[str(data_array['error']['rpi_pixhawk'][0])+'_timestamp'] = now
            #     else:
            #         print(data_array['error'])
            #     # print(data_array)
            else:
                print('unknow data')
                print(data_array['error'])
        else:
            print('not an error message')
            print(data_array)
    else:
        print('not a dictionary file')
        print(data_array)

    print(datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"))
    print('=======================rpi_sensors=========================')
    for rpi_sensors_key in rpi_sensors.keys():
        if '_timestamp' in rpi_sensors_key:
            true_key = rpi_sensors_key.replace('_timestamp', '')

            # check if it still walid to show
            if now - rpi_sensors[rpi_sensors_key] < rpi_sensors[true_key + '_wait'][1]*1000:
                #check if its recent error
                if now - rpi_sensors[rpi_sensors_key] < rpi_sensors[true_key+'_wait'][0]*1000:
                    print("\033[1;31;40m"+'NEW'),
                else:
                    print('OK '),
                print(rpi_sensors[true_key]),
                print("\033[1;37;40m")
    # print('=======================rpi_pixhawk=========================')
    # for rpi_pixhawk_key in rpi_pixhawk.keys():
    #     if '_timestamp' in rpi_pixhawk_key:
    #         true_key = rpi_pixhawk_key.replace('_timestamp', '')
    #
    #         # check if it still walid to show
    #         if now - rpi_pixhawk[rpi_pixhawk_key] < rpi_pixhawk[true_key + '_wait'][1]*1000:
    #             # check if its recent error
    #             if now - rpi_pixhawk[rpi_pixhawk_key] < rpi_pixhawk[true_key + '_wait'][0]*1000:
    #                 print("\033[1;31;40m"+'NEW'),
    #             else:
    #                 print('OK '),
    #             print(rpi_pixhawk[true_key]),
    #             print("\033[1;37;40m")
    print('============================================================')
