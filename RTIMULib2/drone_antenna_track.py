#!/usr/bin/python

import select
import sys
import time
import math
import socket
import datetime
from threading import Thread
import signal
import Adafruit_PCA9685
import RPi.GPIO as GPIO
import json

# import subprocess

TWO_PI = 2 * math.pi
mag_declination = 7  # 7 #degrees
heading_diff = 270  # degrees

read_stdin = 1  # 1-read stdin, 0 - stop
imu_data = [i for i in range(4)]  # pitch, yaw, pitch_available, yaw_available

# long, lat, alt, time_stamp
# external_gps = [23.975045, 54.905929, 5.0, 0]  # IKI x,y,z
external_gps = [23.970201, 54.901221, 5.0, 0]  # Barsausko
# external_gps = [23.967351, 54.905344, 5.0, 0]  # KTU
# long, lat, alt, time_stamp
internal_gps = [23.969979, 54.905414, 0.0, 1]  # salu 2

# raise SystemExit


tilt_pwm_default = 400  # 45 deg
tilt_pwm_max = 600  # 90 deg
tilt_pwm_min = 100

yaw_gpio1 = 17
yaw_gpio2 = 27

tilt_target = 40
yaw_target = 0


def signal_handler(signal, frame):
    global read_stdin
    read_stdin = 0
    print('done')
    time.sleep(2)
    sys.exit(0)


def read_stdin(i):  # read stdin line by line and write sensor pitch, yaw data to array
    global imu_data

    while read_stdin:
        new_data = sys.stdin.readline().split()
        try:
            if new_data[0] == ":":
                imu_data[0] = float(new_data[1])
                sensor_yaw = float(new_data[2])
                sensor_yaw = sensor_yaw + mag_declination + heading_diff
                if sensor_yaw > 360:
                    sensor_yaw = sensor_yaw - 360
                imu_data[1] = sensor_yaw
                imu_data[2] = 1
                imu_data[3] = 1
        except:
            print('reading stdin done')


def calculate_angles(i):
    global tilt_target
    global yaw_target

    while read_stdin:
        print "internal gps:", internal_gps
        print "external gps:", external_gps

        ext_lat = math.radians(external_gps[1])
        ext_lon = math.radians(external_gps[0])
        int_lat = math.radians(internal_gps[1])
        int_lon = math.radians(internal_gps[0])

        dlon = int_lon - ext_lon
        dlat = int_lat - ext_lat

        z_diff = external_gps[2] - internal_gps[2]

        p1 = dlon * math.cos((int_lat + ext_lat) / 2)
        p2 = dlat
        distance = 6371000 * math.sqrt(p1 * p1 + p2 * p2)

        print (str("distance: ") + str(distance) + "z_diff: " + str(z_diff))

        tilt_target = math.degrees(math.atan2(z_diff, distance))

        brng = math.degrees(math.atan2(dlon, dlat))

        yaw_target = (brng + 360) % 360

        print "yaw target: ", yaw_target
        time.sleep(0.5)


# get antenna and drone gps  coordinates
#
def get_drone_and_antenna_coords(i):
    UDP_IP = "192.168.11.1"
    UDP_PORT = 6002

    sock = socket.socket(socket.AF_INET,  # Internet
                         socket.SOCK_DGRAM)  # UDP
    sock.bind((UDP_IP, UDP_PORT))

    global external_gps
    global internal_gps

    while read_stdin:
        is_readable = [sock]  #
        is_writable = []  # for checking if data available
        is_error = []  #
        r, w, e = select.select(is_readable, is_writable, is_error, 1.0)
        if r:
            data, addr = sock.recvfrom(1024)

            try:
                data_array = json.loads(data)
            except:
                data_array = None

            if data_array is not None:
                # long, lat, alt, time_stamp
                external_gps[0] = data_array['external_gps']['long']
                external_gps[1] = data_array['external_gps']['lat']
                external_gps[2] = data_array['external_gps']['alt']
                external_gps[3] = datetime.datetime.now().time()

                internal_gps[0] = data_array['internal_gps']['long']
                internal_gps[1] = data_array['internal_gps']['lat']
                internal_gps[2] = 0.0  # altitude - absolete
                internal_gps[3] = datetime.datetime.now().time()
                print data

        else:
            time.sleep(0.5)


# read gps data from antenna gps receiver and push to variables
# absolete function
# def get_internal_coords(i):
#     UDP_IP = "192.168.11.1"
#     UDP_PORT = 6001
#
#     sock = socket.socket(socket.AF_INET,  # Internet
#                          socket.SOCK_DGRAM)  # UDP
#     sock.bind((UDP_IP, UDP_PORT))
#
#     global internal_gps
#
#     while read_stdin:
#         is_readable = [sock]  #
#         is_writable = []  # for checking if data available
#         is_error = []  #
#         r, w, e = select.select(is_readable, is_writable, is_error, 1.0)
#
#         if r:
#             data, addr = sock.recvfrom(1024)
#             data_array = data.split()
#             if data_array[0] == "internal_gps":
#                 internal_gps[0] = float(data_array[1])
#                 internal_gps[1] = float(data_array[2])
#                 internal_gps[2] = float(data_array[3])
#                 internal_gps[3] = float(data_array[4])
#                 internal_gps[4] = datetime.datetime.now().time()
#         else:
#             time.sleep(0.5)


def servo_turn(i):
    global imu_data

    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)
    GPIO.setup(yaw_gpio1, GPIO.OUT)
    GPIO.setup(yaw_gpio2, GPIO.OUT)
    GPIO.output(yaw_gpio1, GPIO.LOW)
    GPIO.output(yaw_gpio2, GPIO.LOW)
    time.sleep(1)

    rotate_direction = 0
    old_direction = rotate_direction

    while read_stdin:
        if imu_data[3]:  # check if new data available
            imu_data[3] = 0  # mark that this data is absolete
            current_position = imu_data[1]
            distance = yaw_target - current_position
            # distance = 0 - current_position

            # calculate distace between target ant current position and decide if we neet to turn
            sqrt_distance = math.sqrt(distance * distance)

            if (sqrt_distance > 6) and (sqrt_distance < 354):  # distance is greater than +-3 deg, so lets turn

                if distance > 0:
                    turn_left = -1

                else:
                    turn_left = 1

                if sqrt_distance > 180:  # check if we are at the other haff side
                    turn_left = -1 * turn_left  # if so, turn the direction

                if turn_left == -1:
                    rotate_direction = 1  # print "turning left, pwm: ", yaw_pwm_max
                else:
                    rotate_direction = -1  # print "turning right pwm: ", yaw_pwm_min

            else:
                rotate_direction = 0

            if rotate_direction != old_direction:
                old_direction = rotate_direction
                if rotate_direction == 1:
                    GPIO.output(yaw_gpio1, GPIO.HIGH)
                    GPIO.output(yaw_gpio2, GPIO.LOW)
                if rotate_direction == -1:
                    GPIO.output(yaw_gpio1, GPIO.LOW)
                    GPIO.output(yaw_gpio2, GPIO.HIGH)
                if rotate_direction == 0:
                    GPIO.output(yaw_gpio1, GPIO.LOW)
                    GPIO.output(yaw_gpio2, GPIO.LOW)

            time.sleep(0.05)
        else:
            time.sleep(0.05)

    GPIO.output(yaw_gpio1, GPIO.LOW)
    GPIO.output(yaw_gpio2, GPIO.LOW)
    time.sleep(1)


# comparing calculated position tu current imu_data sensor position and set pwm angle
def servo_tilt(i):
    global imu_data

    current_pwm = tilt_pwm_default
    pwm = Adafruit_PCA9685.PCA9685(0x40)
    pwm.set_pwm_freq(50)

    duty = ((current_pwm / 100) + 2.0) / 100
    pwm.set_pwm(5, 0, int(4096.0 * duty))

    time.sleep(1)

    pwm_per_deg = float(tilt_pwm_max - tilt_pwm_min) / 90

    while read_stdin:
        if imu_data[2]:  # check if new data available
            imu_data[2] = 0  # mark that this data is absolete
            print str(tilt_target + 3) + " < " + str(imu_data[0]) + " < " + str(tilt_target - 3)
            if (tilt_target + 3) < imu_data[0]:  # if imu shows to high

                current_pwm = current_pwm - (pwm_per_deg * 2.0)

                if current_pwm < tilt_pwm_min:  # dont let pwm go to low
                    current_pwm = tilt_pwm_min

                print "new pwm" + str(current_pwm)
                duty = ((current_pwm / 100) + 2.0) / 100
                pwm.set_pwm(5, 0, int(4096.0 * duty))

            elif (tilt_target - 3) > imu_data[0]:  # if imu show to low than required target

                current_pwm = current_pwm + (pwm_per_deg * 2.0)

                if current_pwm > tilt_pwm_max:  # dont let pwm go to high
                    current_pwm = tilt_pwm_max

                print "new pwm" + str(current_pwm)
                duty = ((current_pwm / 100) + 2.0) / 100
                pwm.set_pwm(5, 0, int(4096.0 * duty))

            time.sleep(0.05)
        else:
            time.sleep(0.1)


# subprocess.call("python gps_data.py > /dev/null 2>&1 &")
# subprocess.Popen(["./gps_data.py"])


read_sensor_data = Thread(target=read_stdin, args=(1,))
read_sensor_data.start()

tilt_controller = Thread(target=servo_tilt, args=(1,))
tilt_controller.start()

turn_controller = Thread(target=servo_turn, args=(1,))
turn_controller.start()

external_coords_receiver = Thread(target=get_drone_and_antenna_coords, args=(1,))
external_coords_receiver.start()

# internal_coords_receiver = Thread(target=get_internal_coords, args=(1,))
# internal_coords_receiver.start()

calculations_thread = Thread(target=calculate_angles, args=(1,))
calculations_thread.start()

signal.signal(signal.SIGINT, signal_handler)
signal.pause()
