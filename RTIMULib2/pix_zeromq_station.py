#!/usr/bin/python3

#   Hello World server in Python
#   Binds REP socket to tcp://*:5555
#   Expects b"Hello" from client, replies with b"World"
#

import os
import _thread
import time
import zmq
import json
import socket as nativeSocket
import select
import subprocess
import signal
from subprocess import Popen
#import psutil
import datetime

UDP_IP_antenna_tracker = "192.168.11.1"  # send drone gps to this ip
UDP_PORT_antenna_tracker = 6002  # send drone gps to this port

# UDP_IP_antenna_gps = "192.168.11.1"  # get antenna gps to this ip
# UDP_PORT_antenna_gps = 6003  # get antenna gps to this port

#UDP_IP_video_server = ["127.0.0.1"]#None  # send additional data for video processing. "ip" - force ipaddr to listen, None - get ip by reguests from video server
UDP_IP_video_server = None  # send additional data for video processing. "ip" - force ipaddr to listen, None - get ip by reguests from video server
UDP_PORT_SEND_video_server = 6005  # video server data (battery voltages) port to send
UDP_PORT_GET_video_server = 6005  # get controll (drone angless) data from video server data port
UDP_PORT_SEND_errors_server = 6004  # send errors to error processing/view port
UDP_ip_SEND_errors_server = '127.0.0.1'  # send errors to error processing/view IP

request_list = []  # buffer of requests to send to quad
last_command = ''
pix_temperatures = {}
pix_voltage = {'voltage': 0.0, 'cells': {'1': 0.0,
                                         '2': 0.0,
                                         '3': 0.0,
                                         '4': 0.0,
                                         '5': 0.0,
                                         '6': 0.0,
                                         '7': 0.0,
                                         '8': 0.0}
               }
pix_amperage = {'total': 0, 'current': 0}
pix_responce = int(time.time() * 1000)
pix_hearbeat = " ... "
pix_system_state = " ... "
pix_arm_msg = " ... "
pix_vehicle_mode = " ... "
pix_doing_state = " ... "
pix_altitude = 0.0
pix_latitude = 0.0
antenna_longitude = 0.0000
antenna_latitude = 0.0000
pix_longitude = 0.0
pix_gps_fix = False
pix_gps_num_sat = 0
pix_obey_rc_input = " ... "
rc_channels = {"roll": 0, 'pitch': 0, 'throttle': 0, 'yaw': 0, '5th': 0, 'cam': 0}
to_pix_rc_data = {"roll": 0, 'pitch': 0, 'throttle': 0, 'yaw': 0, '5th': 0, 'cam': 0}
start_time = time.time()  #start time of this script ins seconds (float)
debug = 2

send_receive_video_data = 0.25

ts = time.time()
data_file = open('/tmp/pix_zeromq_station_' + str(ts), 'w')
v_ampfile = open('/tmp/pix_v_amp_' + str(ts), 'w')
cycling = False

timestamps = {'pix_amperage': 0, 'pix_voltage': 0, 'pix_system_state': 0, 'pix_arm_msg': 0, 'pix_vehicle_mode': 0,
              'pix_doing_state': 0, 'pix_altitude': 0, 'pix_latitude': 0, 'pix_longitude': 0, 'pix_gps_fix': 0,
              'pix_gps_num_sat': 0, 'rc_channels': 0, 'pix_obey_rc_input': 0, 'pix_temperatures': 0}

# socket to send errors to
errors_processing_sock = nativeSocket.socket(nativeSocket.AF_INET, nativeSocket.SOCK_DGRAM)


def lg(thread_name, log_message, level):
    global data_file
    if level <= debug:
        data_file.write(str(datetime.datetime.now()))
        data_file.write(thread_name+">>>")
        data_file.write(str(log_message))
        data_file.write("\r\n")


def start_antenna_tracker(threadName, data):
    while cycling:
        # a_pid = Popen(["./start_gps_service.sh"])
        # # s_path = "/home/pi/rpi_zeromq_server/RTIMULib2"
        # # a_pid = Popen(s_path+"/RTIMULibDrive | "+s_path+"/drone_antenna_track.py >> /tmp/gps_controll_service_log 2>&1")
        # time.sleep(0.5)  # wait for connection
        # lg("antenna tracker working", 1)
        # # a_pid_code = a_pid.poll()  # get process status
        # while cycling:
        #     time.sleep(1)
        #     lg("antenna tracker working", 1)
        # # a_pid.terminate()
        # # time.sleep(1)
        # for proc in psutil.process_iter():
        #     # check whether the process name matches
        #     print proc.name()
        #     if proc.name() == "RTIMULibDrive":
        #         proc.kill()
        #     if proc.name() == "drone_antenna_track.py":
        #         proc.kill()
        # lg("antenna tracker DONE", 1)
        time.sleep(1)


def send_data_to_video_server(threadName, data):  # send voltage and so other data to server for video rendering
    video_server_sock = nativeSocket.socket(nativeSocket.AF_INET, nativeSocket.SOCK_DGRAM)
    thiz = "send_data_to_video_server"
    while UDP_IP_video_server is None and cycling:
        lg(thiz, "waiting for UDP_IP_video_server", 1)
        time.sleep(1)
    while cycling:
        lg(thiz, "sending udp to UDP_IP_video_server", 3)
        send_string = ':'  # form string to send to cideo server
        #print 'sending to video server'
        # cell cell_min_v <3.7
        for cell_no in pix_voltage['cells']:
            send_string +=str(pix_voltage['cells'][cell_no])+' |'
            if pix_voltage['cells'][cell_no] < 3.7:
                send_string += '1'
            else:
                send_string += '0'
        send_string+=   ' V: '+str(pix_voltage['voltage'])+' |0' + \
                        ' Ah: ' + str(round(pix_amperage['total'], 2))+' |0' + \
                        ' Mode - '+ pix_vehicle_mode + '|0'
        video_server_sock.sendto(send_string.encode(), (UDP_IP_video_server[0], UDP_PORT_SEND_video_server))
        time.sleep(send_receive_video_data)  # the faster we send data to video server - the faster we get rc responses
    video_server_sock.close()

# takes input signal an converts to new signal values
# returs false if input is out of bound
# fix - fix values for not critical data like camera angle
def convert_rc_data_to_pix_data(rc_input, rc_min, rc_max, pix_min, pix_max, blank_spot, reverse_pwm, debug, fix = False):
    thiz = "convert_rc_data_to_pix_data"
    lg(thiz, "START debugging sanitizing rc ", debug)
    try:
        rc_input = float(rc_input)
    except ValueError:
        print('converting rc_input data to float error')
        print(rc_input)
        return False
    if rc_input < rc_min:
        lg(thiz, "got bad data from server, input channel out of boundaries:" + str(rc_input), 1)
        if fix:
            rc_input = rc_min
        else:
            return False

    if rc_input > rc_max:
        lg(thiz, "got bad data from server, input channel out of boundaries:" + str(rc_input), 1)
        if fix:
            rc_input = rc_max
        else:
            return False

    lg(thiz, rc_input, debug)

    rc_input = rc_input - rc_min + ((rc_min - rc_max)/2)  # calculate input diviation from 0
    rc_min = (rc_min - rc_max) / 2  # calculate diviation min allowed
    rc_max = rc_min * -1  # calculate diviations max allowed

    lg(thiz, rc_input, debug)

    if (blank_spot * -1) < rc_input < blank_spot:  # if rc_input is near center, assume it's in center
        rc_input = 0

    # convert input diviation to rc pwm values
    pix_unit_per_rc_unit = (pix_max - pix_min)/(rc_max - rc_min)
    new_rc_input = (rc_input + (rc_max - rc_min)/2) * pix_unit_per_rc_unit + pix_min

    if reverse_pwm:  #if rewerse pwm reguired
        new_rc_input = pix_max + pix_min - new_rc_input
    lg(thiz, new_rc_input, debug)
    lg(thiz, int(new_rc_input), debug)
    lg(thiz, "END debugging sanitizing rc ", debug)

    return int(new_rc_input)

#gets udp packet containing rc data from video server, checks, converts and adds to queue
#then waits for new udp packet
def get_data_from_video_server(threadName, data):  # receiving controll data from video server
    global UDP_IP_video_server
    global request_list
    global to_pix_rc_data

    thiz = "get_data_from_video_server"

    last_good_data_timestamp = 0  # last know good data received from video server
    invalid_threshold_ms = 5000  # if invalid data time received mo then this ms
    same_threshold_ms = 10000  # if same data received more then this ms time

    default_pw = "1500"
    default_cam = "0"

    #  rc_data =  yaw, cam, x, y, z
    rc_data = [default_pw, default_cam, default_pw, default_pw, default_pw]

    sock_controll = nativeSocket.socket(nativeSocket.AF_INET, nativeSocket.SOCK_DGRAM)
    sock_controll.bind(('192.168.11.1', UDP_PORT_GET_video_server))
    last_controll_data = "some data"
    last_controller_cmd_sent = 'guided'

    while cycling:
        controll_data_tmp, UDP_IP_video_server = sock_controll.recvfrom(1024)  # receiving controll data
        lg(thiz, "got raw data from video server", 1)
        lg(thiz, controll_data_tmp, 3)
        controll_data = controll_data_tmp.decode().split(";")

        if controll_data[0] != last_controll_data: # if this is unique new data

            if controll_data[0][0] == ":":  # valid command starts with ":"

                last_controll_data = controll_data[0]
                same_data_threshold = 0  # got new data , so reset threshold
                # we get data in this order:
                # yaw pitch roll x y z ; yaw[-50deg:50deg] pitch[-90deg:90deg] roll[-50deg:50deg](unused) x[-150:150] y[-150:150] z[300:700]
                rc_data = last_controll_data[1:].split(" ")
                rc_data_tmp = rc_data[:]

                valid_rc_input = True
                lg(thiz, 'yaw, pitch, roll, x(+-150),y(+-150),z(300:700,[some other])',1)
                lg(thiz, rc_data, 1)
                # function       convert_rc_data_to_pix_data(rc_input, rc_min, rc_max, pix_min, pix_max, blank_spot, reverse_pwm, debug, fix = False):
                #sanitizing yaw
                rc_data[0] = str(convert_rc_data_to_pix_data(rc_data[0], -30.0, 30.0, 1450.0, 1550.0, 10.0, True, 3, True))
                if rc_data[0] is False:
                    valid_rc_input = False

                # sanitizing camera pitch
                rc_data[1] = str(convert_rc_data_to_pix_data(rc_data[1], -90.0, 90.0, -90.0, 90.0, 0, False, 3, True))
                # if rc_data[1] == '0':
                #     valid_rc_input = False

                # sanitizing x (roll)
                rc_data[3] = str(convert_rc_data_to_pix_data(rc_data[3], -150.0, 150.0, 1300.0, 1700.0, 10.0, False, 3, True))
                if rc_data[3] is False:
                    valid_rc_input = False

                # sanitizing y (throttle)
                rc_data[4] = str(convert_rc_data_to_pix_data(rc_data[4], -100.0, 100.0, 1300.0, 1700.0, 10.0, False, 3, True))
                if rc_data[4] is False:
                    valid_rc_input = False

                # sanitizing z (pitch)
                rc_data[5] = str(convert_rc_data_to_pix_data(rc_data[5], -700.0, -300.0, 1400.0, 1600.0, 10.0, False, 3, True))
                if rc_data[5] is False:
                    valid_rc_input = False

                # additional command
                if len(rc_data) > 6:
                    # send new command if one's registered
                    if rc_data[6] in ['poshold','rtl','land','guided'] and rc_data[6] not in [last_controller_cmd_sent] :
                        last_controller_cmd_sent = rc_data[6]
                        request_list.append(rc_data[6])

                # if controll_data[0].startswith(':0 0 0 0 0 0'):  #if data empty
                # print(rc_data_tmp)
                if rc_data_tmp[0] == '0' and rc_data_tmp[2] == '0' and rc_data_tmp[3] == '0' and \
                        rc_data_tmp[4] == '0' and rc_data_tmp[5] in ['0','-100','-200','-300','-400','-500','-600','-700']:
                    # print ('data in defaults')

                    valid_rc_input = True
                    rc_data[0] = default_pw
                    #  rc_data[1] = default_cam
                    rc_data[3] = default_pw
                    rc_data[4] = default_pw
                    rc_data[5] = default_pw

                if valid_rc_input:  # if everyting is ok
                    last_good_data_timestamp = int(time.time() * 1000)

                    lg(thiz, 'rc input is valid', 3)
                    to_pix_rc_data={'roll': rc_data[3], 'pitch': rc_data[5], 'throttle': rc_data[4],
                                    'yaw': rc_data[0], '5th': 0, 'cam': rc_data[1]}

                    # print('========================================================')
                    # print(rc_data_tmp)
                    # print(to_pix_rc_data)


                    request_list.append(
                        "rc_1234 p" + str(to_pix_rc_data['pitch']) + " r" + str(to_pix_rc_data['roll']) +
                        " y" + str(to_pix_rc_data['yaw']) + " t" + str(to_pix_rc_data['throttle']) +
                        " c" + str(to_pix_rc_data['cam']))

                    lg(thiz, "sending RC to drone>>" + "rc_1234 p" + rc_data[5] + " r" + rc_data[3] + " y" +
                       rc_data[0] + " t" + rc_data[4] + " c" + rc_data[1] + "<<", 1)

                # if not ok but invalid ms threshold is not reached
                elif (int(time.time() * 1000) - last_good_data_timestamp) < invalid_threshold_ms:
                    lg(thiz, 'got invalid data data, setting defaults, except cam', 1)
                    rc_data[0] = default_pw
                    # rc_data[1] = default_cam
                    rc_data[3] = default_pw
                    rc_data[4] = default_pw
                    rc_data[5] = default_pw
                    to_pix_rc_data = {'roll': rc_data[3], 'pitch': rc_data[5], 'throttle': rc_data[4],
                                      'yaw': rc_data[0], '5th': 0, 'cam': rc_data[1]}
                    request_list.append(
                        "rc_1234 p" + str(to_pix_rc_data['pitch']) + " r" + str(to_pix_rc_data['roll']) +
                        " y" + str(to_pix_rc_data['yaw']) + " t" + str(to_pix_rc_data['throttle']) +
                        " c" + str(to_pix_rc_data['cam']))

                # data invalid, and for to long. Dont propogate rc_inpust to drone
                else:
                    lg(thiz, 'FAILSAFE[get_data_from_video_server]: rc input validation failed, data is not valid!!!', 1)

            else:
                # invalid data received
                if (int(time.time() * 1000) - last_good_data_timestamp) < invalid_threshold_ms:
                    # set defaults
                    lg(thiz, "garbled data from video server received>>" + controll_data_tmp + "<<", 1)
                    rc_data[0] = default_pw
                    # rc_data[1] = default_cam
                    rc_data[3] = default_pw
                    rc_data[4] = default_pw
                    rc_data[5] = default_pw
                    to_pix_rc_data = {'roll': rc_data[3], 'pitch': rc_data[5], 'throttle': rc_data[4],
                                      'yaw': rc_data[0], '5th': 0, 'cam': rc_data[1]}
                    request_list.append(
                        "rc_1234 p" + str(to_pix_rc_data['pitch']) + " r" + str(to_pix_rc_data['roll']) +
                        " y" + str(to_pix_rc_data['yaw']) + " t" + str(to_pix_rc_data['throttle']) +
                        " c" + str(to_pix_rc_data['cam']))

        # checking if duplicated data not exceedeing same data time threshold. if not - append last inputs
        elif (int(time.time() * 1000) - last_good_data_timestamp) < same_threshold_ms:
            request_list.append(
                "rc_1234 p" + str(to_pix_rc_data['pitch']) + " r" + str(to_pix_rc_data['roll']) +
                " y" + str(to_pix_rc_data['yaw']) + " t" + str(to_pix_rc_data['throttle']) +
                " c" + str(to_pix_rc_data['cam']))

        # got duplicated data far for to long
        else:
            lg(thiz, 'FAILSAFE[get_data_from_video_server]: rc input is same for too long, not sending', 1)


def ping_pong(threadName, data):
    global request_list
    global cycling

    while True:
        if cycling and not request_list and (int(time.time() * 1000) - pix_responce) > 1000:
            request_list.append('ping')
        time.sleep(0.5)

###################################################
# sends drone gps and antenna gps to antenna tracker
# and requests drone gps from drone
def get_drone_gps_data(threadName, data):
    global request_list

    thiz = "get_drone_gps_data"

    cycling
    sock = nativeSocket.socket(nativeSocket.AF_INET, nativeSocket.SOCK_DGRAM)

    while cycling:
        request_list.append('get_drone_gps')  # aquire gps position
        time.sleep(1)
        if pix_latitude != 0 and antenna_latitude !=0 :  # if we have any data, send to antenna tracker
            gpsString = json.dumps({
                'external_gps':{'lat':pix_latitude, 'long':pix_longitude, 'alt':pix_altitude},
                'internal_gps':{'lat':antenna_latitude, 'long': antenna_longitude}
                 })
            # gpsString = 'external_gps ' + str(pix_longitude) + ' ' + str(pix_latitude) + ' ' + str(pix_altitude)
            sock.sendto(gpsString.encode(), (UDP_IP_antenna_tracker, UDP_PORT_antenna_tracker))
            lg(thiz, "sending drone coords: "+gpsString, 3)


# sends antenna gps data to drone
# absollete functionality
# def send_antenna_gps_data(threadName, data):
#     global request_list
#     global cycling
#     sock = nativeSocket.socket(nativeSocket.AF_INET, nativeSocket.SOCK_DGRAM)
#     sock.bind((UDP_IP_antenna_gps, UDP_PORT_antenna_gps))
#
#     skip = 5
#     skipped = 4
#
#     while cycling:
#         is_readable = [sock]  #
#         is_writable = []  # for checking if data available
#         is_error = []  #
#         r, w, e = select.select(is_readable, is_writable, is_error, 1.0)
#
#         if r:
#             skipped = (skipped + 1) % skip
#             if (skipped == 0):
#                 data, addr = sock.recvfrom(1024)
#                 data_array = data.split()
#                 if (data_array[0] == "internal_gps"):
#                     antenna_gps_long = float(data_array[1])
#                     antenna_gps_lat = float(data_array[2])
#                     # request_list.append('external_gps '+:{antenna_gps_long, 'lat':antenna_gps_lat) #send antenna gps position
#
#         else:
#             time.sleep(0.5)
#         time.sleep(1)
#         request_list.append('set_station_gps 23.970015 54.905412')  # send antenna gps position


#########################################
# wait for user inputs and append to request list
def read_input_request(threadName, data):
    global request_list
    global last_command
    global antenna_longitude
    global antenna_latitude
    thiz = "read_input_request"
    while True:
        req = input()
        if req.startswith('load'):
            file_name = req.split(' ')
            if len(file_name[1]) > 0:
                try:
                    with open(file_name[1] + '.json') as f:
                        j_data = json.load(f)
                    antenna_latitude=j_data['lat']
                    antenna_longitude = j_data['long']
                    with open('default.json', 'w') as f:
                        json.dump({'long': antenna_longitude, 'lat': antenna_latitude}, f)
                except:
                    lg(thiz, 'can not load antenna gps data file or data',1)
                    lg(thiz, data,1)
        elif req.startswith('save'):
            file_name = req.split(' ')
            # save antenna gps data to named file, and default_file
            if len(file_name[1]) > 0:
                with open(file_name[1]+'.json', 'w') as f:
                    json.dump({'long':pix_longitude, 'lat':pix_latitude}, f)
                with open('default.json', 'w') as f:
                    json.dump({'long': pix_longitude, 'lat': pix_latitude}, f)
                    # lg(thiz, data,1)
        else:
            request_list.append(req)
            last_command = req


########################################

# processes data got from drone
def collect_data(message):
    global pix_temperatures
    global pix_voltage
    global pix_amperage
    global pix_hearbeat
    global pix_system_state
    global pix_arm_msg
    global pix_vehicle_mode
    global pix_doing_state
    global pix_altitude
    global pix_latitude
    global pix_longitude
    global pix_gps_fix
    global pix_gps_num_sat
    global pix_obey_rc_input
    global rc_channels
    global timestamps

    thiz = "collect_data"

    lg(thiz, message, 3)

    got_voltage = False

    if message:
        for data in message:

            if 'current' in data:
                pix_amperage = data['current']
                timestamps['pix_amperage'] = int(time.time() * 1000)

            if 'voltage' in data:
                pix_voltage['voltage'] = data['voltage']['V']
                pix_voltage['cells']['1'] = data['voltage']['0']
                pix_voltage['cells']['2'] = data['voltage']['1']
                pix_voltage['cells']['3'] = data['voltage']['2']
                pix_voltage['cells']['4'] = data['voltage']['3']
                pix_voltage['cells']['5'] = data['voltage']['4']
                pix_voltage['cells']['6'] = data['voltage']['5']
                pix_voltage['cells']['7'] = data['voltage']['6']
                pix_voltage['cells']['8'] = data['voltage']['7']
                timestamps['pix_voltage'] = int(time.time() * 1000)
                lg(thiz, pix_voltage, 3)
                got_voltage = True

            if 'temperatures' in data:
                timestamps['pix_temperatures'] = int(time.time() * 1000)
                pix_temperatures = data['temperatures']
                lg(thiz, pix_temperatures, 3)
                got_temperatures = True

            if 'error' in data:
                errors_processing_sock.sendto(json.dumps(data).encode(), (UDP_ip_SEND_errors_server, UDP_PORT_SEND_errors_server))

            if 'system_state' in data:
                lg(thiz, "STATE!!!!: " + data['system_state'], 2)
                pix_system_state = data['system_state']
                timestamps['pix_system_state'] = int(time.time() * 1000)

            if 'arm_msg' in data:
                lg(thiz, "ARM!!!", 2)
                pix_arm_msg = data['arm_msg']
                timestamps['pix_arm_msg'] = int(time.time() * 1000)

            if 'doing_state' in data:
                pix_doing_state = data['doing_state']
                timestamps['pix_doing_state'] = int(time.time() * 1000)

            if 'alt' in data:
                pix_altitude = data['alt']
                timestamps['pix_altitude'] = int(time.time() * 1000)
                lg(thiz, pix_altitude, 3)

            if 'lat' in data:
                pix_latitude = data['lat']
                timestamps['pix_latitude'] = int(time.time() * 1000)
                lg(thiz, pix_latitude, 3)

            if 'long' in data:
                pix_longitude = data['long']
                timestamps['pix_longitude'] = int(time.time() * 1000)
                lg(thiz, pix_longitude, 3)

            if 'pixhawk_state' in data:

                pix_latitude = data['pixhawk_state']['gps']['lat']
                timestamps['pix_latitude'] = int(time.time() * 1000)
                lg(thiz, pix_latitude, 3)

                pix_longitude = data['pixhawk_state']['gps']['long']
                timestamps['pix_longitude'] = int(time.time() * 1000)
                lg(thiz, pix_longitude, 3)

                pix_altitude = data['pixhawk_state']['gps']['alt']
                timestamps['pix_altitude'] = int(time.time() * 1000)
                lg(thiz, pix_altitude, 3)

                pix_gps_fix = data['pixhawk_state']['gps_sats']['gps_fix']
                timestamps['pix_gps_fix'] = int(time.time() * 1000)

                pix_gps_num_sat = data['pixhawk_state']['gps_sats']['gps_num_sat']
                timestamps['pix_gps_num_sat'] = int(time.time() * 1000)

                pix_vehicle_mode = data['pixhawk_state']['vehicle_mode']
                if pix_vehicle_mode is not None:
                    pix_vehicle_mode = pix_vehicle_mode.lower()
                else:
                    pix_vehicle_mode = 'Unknown'
                timestamps['pix_vehicle_mode'] = int(time.time() * 1000)

                pix_system_state = data['pixhawk_state']['system_state']
                if pix_system_state is None:
                    pix_system_state = 'Unknown'
                timestamps['pix_system_state'] = int(time.time() * 1000)

                pix_arm_msg = data['pixhawk_state']['state']
                timestamps['pix_arm_msg'] = int(time.time() * 1000)

                pix_obey_rc_input = data['pixhawk_state']['obey_rc_input']
                if pix_obey_rc_input is None:
                    pix_obey_rc_input = 'Unknown'
                timestamps['pix_obey_rc_input'] = int(time.time() * 1000)

            if 'rc_channels' in data:
                rc_channels['roll'] = data['rc_channels']['roll']
                rc_channels['pitch'] = data['rc_channels']['pitch']
                rc_channels['throttle'] = data['rc_channels']['throttle']
                rc_channels['yaw'] = data['rc_channels']['yaw']
                rc_channels['5th'] = data['rc_channels']['5th']
                timestamps['rc_channels'] = int(time.time() * 1000)
        if got_voltage:
            v_ampfile.write(str(int((time.time() - start_time)*1000))+',')
            v_ampfile.write(str(pix_amperage['current'])+ ","+ str(pix_voltage['voltage'])+','+
                            str(pix_voltage['cells']['1'])+','+
                            str(pix_voltage['cells']['2'])+','+
                            str(pix_voltage['cells']['3'])+','+
                            str(pix_voltage['cells']['4'])+','+
                            str(pix_voltage['cells']['5'])+','+
                            str(pix_voltage['cells']['6'])+','+
                            str(pix_voltage['cells']['7'])+','+
                            str(pix_voltage['cells']['8'])+"\r\n")

        if got_temperatures:
            pass
    return True


def draw_color(date_time):
    if (int(time.time() * 1000) - date_time) > 2000:
        return "\033[1;31;40m"
    else:
        return "\033[1;37;40m"


def draw_data():
    os.system('clear')
    print("\033[1;37;40m"),
    print("+----------------------------------------------")
    print(" | last command         | %s" % last_command)
    print(" | save/load ?,         |")
    print(" | reset,s,h,l,man,dig  |-----------------------")
    print("+----------------------------------------------")
    print("| antenna gps long, lat| %s, %s" % (round(antenna_longitude,4), round(antenna_latitude,4)))
    print("+----------------------------------------------")
    print(draw_color(timestamps['pix_voltage']))
    print("| pix voltage          | %s" % pix_voltage['voltage'])
    print(" | 1 -> %s | 2 -> %s | 3 -> %s | 4 -> %s " % (pix_voltage['cells']['1'], pix_voltage['cells']['2'], pix_voltage['cells']['3'], pix_voltage['cells']['4']))
    print(" | 5 -> %s | 6 -> %s | 7 -> %s | 8 -> %s " % (pix_voltage['cells']['5'], pix_voltage['cells']['6'], pix_voltage['cells']['7'], pix_voltage['cells']['8']))
    print("+----------------------------------------------")
    print(draw_color(timestamps['pix_temperatures']))
    print("|",  end=" ")
    for key in pix_temperatures:
        if (int(key)+1) % 4:
            print("%s->%s, " % (key, pix_temperatures[key]),  end=" ")
        else:
            print("%s->%s, " % (key, pix_temperatures[key]))
            print("|", end=" ")
    print("")
    print("+----------------------------------------------")
    print(draw_color(timestamps['pix_amperage'])),
    print("| pix current          | %s" % round(pix_amperage['current'], 2))
    print("+----------------------------------------------")
    print(draw_color(timestamps['pix_amperage'])),
    print("| pix bat. mAh (reset) | %s" % round(pix_amperage['total'], 2))
    print("+----------------------------------------------")
    print("\033[1;37;40m"),
    print("| pix heartbeat        | %s" % pix_hearbeat)
    print("+----------------------------------------------")
    print(draw_color(timestamps['pix_system_state'])),
    print("| pix sys.state        | %s" % pix_system_state)
    print("+----------------------------------------------")
    print(draw_color(timestamps['pix_arm_msg'])),
    print("| pix arm msg          | %s" % pix_arm_msg)
    print("+----------------------------------------------")
    print(draw_color(timestamps['pix_vehicle_mode'])),
    print("| pix vehic.mode       | %s" % pix_vehicle_mode)
    print("+----------------------------------------------")
    print(draw_color(timestamps['pix_doing_state'])),
    print("| pix doing state      | %s" % pix_doing_state)
    print("+----------------------------------------------")
    print(draw_color(timestamps['pix_altitude'])),
    print("| pix altitude         | %s" % pix_altitude)
    print("+----------------------------------------------")
    print(draw_color(timestamps['pix_latitude'])),
    print("| pix latitude         | %s" % pix_latitude)
    print("+----------------------------------------------")
    print(draw_color(timestamps['pix_longitude'])),
    print("| pix longitude        | %s" % pix_longitude)
    print("+----------------------------------------------")
    print(draw_color(timestamps['pix_gps_fix'])),
    print("| pix GPS fix          | %s" % pix_gps_fix)
    print("+----------------------------------------------")
    print(draw_color(timestamps['pix_gps_num_sat'])),
    print("| pix GPS num.sat      | %s" % pix_gps_num_sat)
    print("+----------------------------------------------")
    print(draw_color(timestamps['rc_channels'])),
    print("|  man rc input  %s  |  Rx   Tx" % pix_obey_rc_input)
    print(" |        roll  1       | %s %s" % (rc_channels['roll'], to_pix_rc_data['roll']))
    print(" |       pitch  2       | %s %s" % (rc_channels['pitch'], to_pix_rc_data['pitch']))
    print(" |    throttle  3       | %s %s" % (rc_channels['throttle'], to_pix_rc_data['throttle']))
    print(" |         yaw  4       | %s %s" % (rc_channels['yaw'], to_pix_rc_data['yaw']))
    print(" |         5th  5       | %s %s" % (rc_channels['5th'], to_pix_rc_data['5th']))
    print(" |         cam  6       | %s %s" % (rc_channels['cam'], to_pix_rc_data['cam']))
    print("+----------------------------------------------")
    print("\033[1;37;40m"),



try:
    with open('default.json') as f:
        data = json.load(f)
    antenna_latitude = data['lat']
    antenna_longitude = data['long']
except:
    lg("main", 'can not load antenna gps data file or data', 1)
    lg(data, 1)

    # print antenna_latitude

context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.bind("tcp://*:5555")


try:
    _thread.start_new_thread(read_input_request, ("read_input_request_thread", 2,))
except:
    print ("Error: unable to start thread |read_input_request|")
    lg("main", "Error: unable to start thread |read_input_request|", 1)

try:
    _thread.start_new_thread(ping_pong, ("ping_pong", 2,))
except:
    print("Error: unable to start thread |ping_pong|")
    lg("main","Error: unable to start thread |ping_pong|", 1)

# start drona tracker process
# p = subprocess.Popen("./RTIMULibDrive | ./drone_antenna_track.py", stdout=subprocess.PIPE, shell=True, preexec_fn=os.setsid)

pong = [{u'heartbeat': u'pong'}]
cycling = True

try:
    _thread.start_new_thread(send_data_to_video_server,  ("send_data_to_video_server", 2,))
except:
    lg("main", "Error: unable to start thread |send_data_to_video_server|", 1)

try:
    _thread.start_new_thread(get_data_from_video_server, ("get_data_from_video_server", 2,))
except:
    lg("main", "Error: unable to start thread |get_data_from_video_server|", 1)

try:
    _thread.start_new_thread(get_drone_gps_data, ("get_drone_gps_thread", 2,))
except:
    lg("main", "Error: unable to start thread |get_gps_data|", 1)

# try:
#     thread.start_new_thread(send_antenna_gps_data, ("send_drone_gps_thread", 2,))
# except:
#     lg("Error: unable to start thread |send_antenna_gps_data|", 1)

try:
    _thread.start_new_thread(start_antenna_tracker, ("start_antenna_tracker", 2,))
except:
    lg("main", "Error: unable to start thread |start_antenna_tracker|", 1)

while cycling:
    if request_list:
        res = request_list[0]  # get first pending request
        request_list.remove(res)  # and remove it from the list

        if res == '9':
            cycling = False

        lg("main", "Sending request", 2)
        lg("main", res, 2)
        socket.send_json(json.dumps({'msg': res}))

        #  Get the reply.
        message = socket.recv_json()
        message = json.loads(message)
        pix_responce = int(time.time() * 1000)
        lg("main", "Received reply", 2)
        lg("main", message, 2)

        # if cmp(message, pong) != 0:
        #     lg("Received reply", 3)
        #     lg(message, 3)
        # else:
        #     lg("received pong replay", 3)

        collect_data(message)

        draw_data()

    else:
        time.sleep(0.3)  # sleep if no request is needed

# os.killpg(os.getpgid(p.pid), signal.SIGINT)

print("done")
time.sleep(5)
data_file.close()
