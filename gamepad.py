#!/usr/bin/python

# :0 0 0 0 0 0


from inputs import get_gamepad
from threading import Thread, Event
import socket as nativeSocket
import time

data = [0,0,0,0,0,-300,'guided']  #yaw, pitch, roll, x(+-150),y(+-150),z(300:700), [additional cmd]
cycle = True


def push_data_out(e_start_now):
    zero_station_server_sock = nativeSocket.socket(nativeSocket.AF_INET, nativeSocket.SOCK_DGRAM)
    while cycle:
        # e_start_now.wait(0.5)  # sleep for .5s or until we get event command
        # e_start_now.clear()
        data_string = ":{0} {1} {2} {3} {4} {5} {6};".format(str(data[0]), str(data[1]), str(data[2]), str(data[3]),
                                                        str(data[4]), str(data[5]), data[6])
        print(data_string)
        time.sleep(0.25)
        zero_station_server_sock.sendto(data_string, ("192.168.11.1", 6005))
    zero_station_server_sock.close()


def calculate_angle(analog, min_angl, max_angl, reverse = False):
    total_angle = max_angl-min_angl
    # print(total_angle)
    angles_per_analog_point = float(total_angle)/255.00
    # print(angles_per_analog_point)
    angle_count = angles_per_analog_point * analog
    # print(angle_count)
    real_angle = angle_count+min_angl
    if reverse:
        real_angle *= -1
    # print(real_angle)
    return int(real_angle)


def main():
    e_start_now = Event()

    push_data_out_t = Thread(target=push_data_out, args=(e_start_now,))
    push_data_out_t.start()

    # data = [0,0,0,0,0,500]  #yaw, pitch, roll, x(+-150),y(+-150),z(300:700)
    """Just print out some event infomation when the gamepad is used."""
    while cycle:
        events = get_gamepad()
        changed = False
        for event in events:
            if event.code == 'ABS_X':  # yaw L=0, R=255, m=128
                last_val = data[0]
                data[0] = calculate_angle(event.state, -41, 41, True)
                if last_val != data[0]:
                    changed = True
            if event.code == 'ABS_Y':  # pitch forward camera F=0, b=255, m=128
                last_val = data[1]
                data[1] = calculate_angle(event.state, -91, 91)
                if last_val != data[1]:
                    changed = True
            if event.code in ['ABS_RX', 'ABS_Z']:  # x L=0, R=255, m=128
                last_val = data[3]
                data[3] = calculate_angle(event.state, -151, 151)
                if last_val != data[3]:
                    changed = True
            if event.code == 'ABS_RZ':  # z F=0, B=255, m=128
                last_val = data[5]
                data[5] = calculate_angle(event.state, -701, -299)
                if last_val != data[5]:
                    changed = True
            if event.code in ['BTN_TRIGGER', 'BTN_SOUTH']:  # y (up) U=1, m=0
                if event.state:
                    data[4] = 101
                else:
                    data[4] = 0
                changed = True
            if event.code in ['BTN_THUMB2', 'BTN_C']:  # y (down) D=1, m=0
                if event.state:
                    data[4] = -101
                else:
                    data[4] = 0
                changed = True

            if event.code in ['BTN_TOP', 'BTN_NORTH']:  # middle button left
                if event.state:
                    data[6] = 'poshold'
                    changed = True

            if event.code in ['BTN_THUMB','BTN_EAST']:  # middle button right
                if event.state:
                    data[6] = 'guided'
                    changed = True

            if event.code in ['BTN_PINKIE', 'BTN_Z']:  # front right button up
                if event.state:
                    data[6] = 'rtl'
                    changed = True

            if event.code in ['BTN_BASE2', 'BTN_TR']:  # front right button down
                if event.state:
                    data[6] = 'land'
                    changed = True

            if changed:
                # print(data)
                e_start_now.set()
            # if event.code not in ['ABS_Z', 'SYN_REPORT']:
            # print(event.ev_type, event.code, event.state)


if __name__ == "__main__":
    main_t = Thread(target=main)
    main_t.start()
    # main()
    while cycle:
        if raw_input() == '9':
            cycle = False
    time.sleep(1)
